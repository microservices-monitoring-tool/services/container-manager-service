﻿namespace MetaLensContainerManagerService.Interface
{
    public interface IContainerService
    {
        Task StartServiceAsync(string containerId);
        Task StopServiceAsync(string containerId);
        Task RestartServiceAsync(string containerId);
    }
}
