﻿using MetaLensContainerManagerService.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MetaLensContainerManagerService.Controller
{
    [AllowAnonymous]
    [ApiController]
    [Route("api/[controller]")]
    public class ContainerController : ControllerBase
    {
        private readonly ILogger<ContainerController> logger;
        private readonly IContainerService containerService;
        private readonly HttpClient httpClient;

        public ContainerController(IContainerService containerService, ILogger<ContainerController> logger, HttpClient httpClient)
        {
            this.logger = logger;
            this.containerService = containerService;
            this.httpClient = httpClient;
            this.httpClient.BaseAddress = new Uri("http://metalensmonitoringservice:80/api/monitoring/stop/");
        }

        [Route("start/{containerId}")]
        [HttpPost]
        public async Task<ActionResult> StartServiceAsync(string containerId)
        {
            try
            {
                await this.containerService.StartServiceAsync(containerId);
                return Ok($"Service {containerId} successfully started");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Error starting service {containerId}");
                return BadRequest($"Error starting service {containerId}");
            }
        }

        [Route("stop/{containerID}")]
        [HttpPost]
        public async Task<ActionResult> StopServiceAsync(string containerId)
        {
            try
            {
                HttpResponseMessage response = await this.httpClient.PostAsync(containerId, null);

                if(response.IsSuccessStatusCode)
                {
                    logger.LogInformation("Service stop state successfully added");
                    await this.containerService.StopServiceAsync(containerId);
                    return Ok($"Service {containerId} successfully stopped");
                }

                logger.LogInformation("Could not add service stop state");
                return BadRequest("Could not stop service");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Error stopping service {containerId}");
                return BadRequest($"Error stopping service {containerId}");
            }
        }

        [Route("restart/{containerId}")]
        [HttpPost]
        public async Task<ActionResult> RestartServiceAsync(string containerId)
        {
            try
            {
                await this.containerService.RestartServiceAsync(containerId);
                return Ok($"Service {containerId} successfully restarted");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Error restarting service {containerId}");
                return BadRequest($"Error restarting service {containerId}");
            }
        }
    }
}
