﻿using Docker.DotNet;
using MetaLensContainerManagerService.Interface;

namespace MetaLensContainerManagerService
{
    public class ContainerService : IContainerService
    {
        private readonly ILogger<ContainerService> logger;
        private readonly IConfiguration configuration;
        private readonly IDockerClient dockerClient;

        public ContainerService(ILogger<ContainerService> logger, IConfiguration configuration, IDockerClient dockerClient)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.dockerClient = dockerClient;
            
        }

        public async Task RestartServiceAsync(string containerId)
        {
            try
            {
                await dockerClient.Containers.RestartContainerAsync(containerId, new Docker.DotNet.Models.ContainerRestartParameters());
                logger.LogInformation($"Successfully stopped container {containerId}");
            }
            catch (Docker.DotNet.DockerContainerNotFoundException)
            {
                logger.LogError($"Error stopping container {containerId}");
            }
            catch (Exception ex)
            {
                logger.LogError($"An error occurred: {ex.Message}");
            }
        }

        public async Task StartServiceAsync(string containerId)
        {
            try
            {
                await dockerClient.Containers.StartContainerAsync(containerId, new Docker.DotNet.Models.ContainerStartParameters());
                logger.LogInformation($"Successfully stopped container {containerId}");
            }
            catch (Docker.DotNet.DockerContainerNotFoundException)
            {
                logger.LogError($"Error stopping container {containerId}");
            }
            catch (Exception ex)
            {
                logger.LogError($"An error occurred: {ex.Message}");
            }
        }

        public async Task StopServiceAsync(string containerId)
        {
            try
            {
                await dockerClient.Containers.StopContainerAsync(containerId, new Docker.DotNet.Models.ContainerStopParameters());
                logger.LogInformation($"Successfully stopped container {containerId}");
            }
            catch (Docker.DotNet.DockerContainerNotFoundException)
            {
                logger.LogError($"Error stopping container {containerId}");
            }
            catch (Exception ex)
            {
                logger.LogError($"An error occurred: {ex.Message}");
            }
        }
    }
}