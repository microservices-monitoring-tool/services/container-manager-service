using Docker.DotNet;
using MetaLens.Monitoring.MessageExporter.Docker;
using MetaLens.Monitoring.MessageExporter.Exporter;
using MetaLens.Monitoring.MessageExporter.Generator;
using MetaLens.Monitoring.MessageExporter.Interface;
using MetaLens.Monitoring.MessageExporter.RabbitMQ;
using MetaLensContainerManagerService;
using MetaLensContainerManagerService.Interface;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddHttpClient();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession();
builder.Services.AddSingleton<IServiceMessageExporter, ServiceMessageExporter>();
builder.Services.AddSingleton<IRabbitMQPublisher, RabbitMQPublisher>();
builder.Services.AddSingleton<IRabbitMQConnector, RabbitMQConnector>();
builder.Services.AddSingleton<IServiceDataGenerator, ServiceDataGenerator>();
builder.Services.AddSingleton<IDockerConnector, DockerConnector>();
builder.Services.AddScoped<IContainerService, ContainerService>();
builder.Services.AddHostedService<MonitoringBackgroundService>();
builder.Services.AddSingleton<IDockerClient>(provider =>
{
    var dockerUri = new Uri("unix:///var/run/docker.sock");
    return new DockerClientConfiguration(dockerUri).CreateClient();
});

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policyBuilder =>
        {
            policyBuilder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
        });
});

builder.Services.AddHttpContextAccessor();

var logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    .Enrich.FromLogContext()
    .WriteTo.Console()
    .CreateLogger();

builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);

var app = builder.Build();

app.UseCors(_ => _.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();
