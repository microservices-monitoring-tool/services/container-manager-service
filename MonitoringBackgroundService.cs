﻿using MetaLens.Monitoring.MessageExporter.Interface;
using Newtonsoft.Json.Linq;

namespace MetaLensContainerManagerService
{
    public class MonitoringBackgroundService : BackgroundService
    {
        private readonly ILogger<MonitoringBackgroundService> logger;
        private readonly IServiceMessageExporter serviceMessageExporter;
        private readonly IRabbitMQConnector rabbitMQConnector;
        private readonly IDockerConnector dockerConnector;
        private Stream containerStatsStream;

        public MonitoringBackgroundService(ILogger<MonitoringBackgroundService> logger, IServiceMessageExporter serviceMessageExporter, IRabbitMQConnector rabbitMQConnector, IDockerConnector dockerConnector)
        {
            this.logger = logger;
            this.serviceMessageExporter = serviceMessageExporter;
            this.rabbitMQConnector = rabbitMQConnector;
            this.dockerConnector = dockerConnector;
        }

        protected async override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            await DoExecuteAsync(cancellationToken);
        }

        private async Task DoExecuteAsync(CancellationToken cancellationToken)
        {
            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    JObject? containerStats = null;

                    var channel = rabbitMQConnector.ConnectToMessageBroker();

                    if (Environment.OSVersion.Platform == PlatformID.Unix)
                    {
                        this.containerStatsStream = await dockerConnector.GetContainerStatsStreamAsync();

                        if (containerStatsStream != null)
                        {
                            var reader = new StreamReader(containerStatsStream, leaveOpen: true);

                            string line;

                            while ((line = await reader.ReadLineAsync()) != null)
                            {
                                try
                                {
                                    containerStats = JObject.Parse(line);

                                    await serviceMessageExporter.ExecuteServiceMessageTaskAsync(channel, containerStats);
                                }
                                catch (Exception ex)
                                {
                                    this.logger.LogError(ex, "Could not push statistics to message exporter");
                                }
                            }
                        }
                    }

                    await serviceMessageExporter.ExecuteServiceMessageTaskAsync(channel, containerStats);
                }

                await Task.Delay(2000, cancellationToken);
            }
            catch (Exception ex)
            {
                logger.LogError($"Exporter assembly was not reachable due to: {ex}");
            }
            finally
            {
                this.containerStatsStream?.Dispose();
            }

            await Task.CompletedTask;
        }
    }
}